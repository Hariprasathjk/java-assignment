package com.Collections;

import java.util.ArrayList;

public class ArrayListPractise {
	public static void main(String[] args) {
		ArrayList<String>list=new ArrayList<>();
		list.add("a");
		list.add("c");
		list.add("b");
		list.add("e");
		list.add("d");
		System.out.println(list);
		System.out.println(list.size());
		for (String s : list) {
			System.out.println(s);
			}
		list.add("f");
		System.out.println(list);
		list.set(2, "b1");
		System.out.println(list);
		list.remove(3);
		System.out.println(list);
		
	}

}
