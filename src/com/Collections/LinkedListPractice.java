package com.Collections;

import java.util.LinkedList;

public class LinkedListPractice {
public static void main(String[] args) {
	LinkedList<String>list=new LinkedList<>();
	list.add("a");
	list.add("c");
	list.add("d");
	list.add("b");
	
	System.out.println(list);
	list.add(1,"a1");
	System.out.println(list);
	
	list.addFirst("a0");
	System.out.println(list);
	list.addLast("g");
	System.out.println(list);
	list.remove("d");
	System.out.println(list);

}
}


//a points to b
//b points to c
//c points to d
//d points to e