package com.String.practise;

public class CharPractise {
	public static void main(String[] args) {
		
		
		
		String str="Welcome";
		String str1="Welcome";
		String str3=new String("Welcome");
		String str4="Java";
		boolean a=str.equals(str1);//true
		boolean b=str.equals(str3);//true
		boolean c=str.equals(str4);//false
		boolean d=str.equals(str3);//true
		

		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
		System.out.println(d);

		//Because the equals() method checks the content but not the reference
			
		boolean a1= str==str1;
		boolean a2= str==str3;
		boolean a3= str==str4;
		boolean a4= str1==str3;
		System.out.println(a1);//true
		System.out.println(a2);//false
		System.out.println(a3);//false
		System.out.println(a4);//false
	}

}
