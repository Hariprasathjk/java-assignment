package com.String.practise;

public class StringConcept {
	public static void main(String[] args) {
		String s1="Welcome";// String constant pool area
		char c[]= {'s','e','l','e','n','i','u','m'};
		String s2="Welcome";//String constant pool area
		String s3=new String("Welcome");//Stored in Heap memory
		String s4=new String("Hello World");// Stored in Heap memory
		
		//char Array to String
		
		String s=new String(c);
		System.out.println(s);
		System.out.println("s1 value "+s1+", s2 value "+s2+" ,s3 value "+s3+",s4 value "+s4);
		s1.concat("Java");
		System.out.println(s1);
		s1=s1.concat(" Java");// declaring the string in the seperate reference
		System.out.println(s1);
	}

}
