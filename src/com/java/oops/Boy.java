package com.java.oops;

public class Boy extends Method_Overriding {
	@Override
	public void boy_Name(String name) {
		
		super.boy_Name(name);  //Parent class reference
		System.out.println("override boy name:"+name);
	}
	public static void main(String[] args) {
		Boy m=new Boy();
		m.boy_Name("Hari");
	}

}
