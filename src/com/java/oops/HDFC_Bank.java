package com.java.oops;

public class HDFC_Bank extends Bank{
	  public void savingAcct() {
		    System.out.println("6%");

		  }

		  public void currentAcct() {
		    System.out.println("8%");
		  }

		  public static void main(String[] args) {
		    Bank b = new HDFC_Bank();
		    b.savingAcct();
		    b.currentAcct();
		    b.branchDetails();
		  }

}
