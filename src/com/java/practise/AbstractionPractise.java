package com.java.practise;

public abstract class AbstractionPractise {
	//An Abstract class consists of both abstract and non abstract methods
	//Non abstract method is not compulsory,it can have all the methods as abstract methods
	//An abstract method is a method without the body, it contains just a declaration
	//We cannot be instantiated that is we cannot created object for that class
	//A child class can implement all the abstract methods present in the parent class(abstract class).
	// what happens if the child does not implements abstract method?
	public abstract void employee();
	public void department() {
		

	}
	

}
