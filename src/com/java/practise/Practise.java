package com.java.practise;

public class Practise {
	int a=0;      // instance variable
	static int b=5;   //static variable
	public void sample() {
		int c=20;// local variable
		a=a+5;
		System.out.println(a);

	}
	
	public static void main(String[] args) {
		//Practise p=new Practise();
		//a=a+20;//we cannot call instance variable without calling the object
		//System.out.println(a);
		b=b+5;  //we can access static varibale without using the object
		System.out.println(b);
		//c=c*2;//the scope of local variable is only in the method
		
		
	}

}
