package com.java.practise;

import java.util.Scanner;
public class SelectedCharacterOccurance {
	public static void main(String[] args) {
	    
			Scanner scanner = new Scanner(System.in);
	        System.out.println("Enter a word: ");
	        String str = scanner.next();
	        System.out.println("Enter a character to find its occurance: ");
	        char c = scanner.next().charAt(0);
	        int count = 0;
	        
	        
	        for (int i = 0; i < str.length(); i++) {
	            if (str.charAt(i) == c) {
	                count++;
	            }
	        }
	        System.out.println("The occurance of '" + c+ "' in '" + str + "' is: " + count);
	    }
	}


